﻿using System;
using System.Windows.Forms;
using System.Diagnostics;
using System.IO;

namespace Compactify {

    public class FileWalker {

        private static TextBox _progressOutput;
        private static string _action;
        private static string _algorithm;


        public static void Start(string startingPoint, TextBox progressOutput, string action, string alg) {
            _action = action;
            _algorithm = alg;
            _progressOutput = progressOutput;

            // Tell the file walker where to start
            System.IO.DirectoryInfo rootDir = new System.IO.DirectoryInfo(startingPoint);
            WalkDirectoryTree(rootDir);

            progressOutput.Text = "Finished!";
        }

        static void WalkDirectoryTree(System.IO.DirectoryInfo root) {
            _progressOutput.Text = root.FullName;

            System.IO.FileInfo[] files = null;
            System.IO.DirectoryInfo[] subDirs = null;

            // First, process all the files directly under this folder
            try {
                files = root.GetFiles("*.*");
            }

            // This is thrown if even one of the files requires permissions greater
            // than the application provides.
            catch (UnauthorizedAccessException e) {
                // Ignore
            }

            catch (System.IO.DirectoryNotFoundException e) {
                // Ignore
            }

            if (files != null) {
                try {
                    Process process = new Process();
                    process.StartInfo = new ProcessStartInfo() {
                        WindowStyle = ProcessWindowStyle.Hidden,
                        FileName = "cmd.exe",
                        RedirectStandardInput = true,
                        //RedirectStandardOutput = true,
                        //RedirectStandardError = true,
                        CreateNoWindow = true,
                        UseShellExecute = false
                    };
                    process.Start();

                    using (StreamWriter sw = process.StandardInput) {
                        foreach (System.IO.FileInfo fi in files) {
                            if (sw.BaseStream.CanWrite) {
                                sw.WriteLine(string.Format("compact.exe {0} {1} {2}", _action, _algorithm, fi.FullName));
                            }
                        }
                    }

                    process.WaitForExit();
                }
                catch (Exception e) {
                    // Ignore
                }

                // Now find all the subdirectories under this directory.
                subDirs = root.GetDirectories();

                foreach (System.IO.DirectoryInfo dirInfo in subDirs) {
                    // Resursive call for each subdirectory.
                    WalkDirectoryTree(dirInfo);
                }
            }
        }
    }
}
