﻿namespace Compactify
{
    partial class frm_main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.grp_step1 = new System.Windows.Forms.GroupBox();
            this.lbl_selected = new System.Windows.Forms.Label();
            this.btn_selectdir = new System.Windows.Forms.Button();
            this.grp_step2 = new System.Windows.Forms.GroupBox();
            this.lbl_compression = new System.Windows.Forms.Label();
            this.rad_compression_insane = new System.Windows.Forms.RadioButton();
            this.rad_compression_high = new System.Windows.Forms.RadioButton();
            this.rad_compression_medium = new System.Windows.Forms.RadioButton();
            this.rad_compression_low = new System.Windows.Forms.RadioButton();
            this.grp_step3 = new System.Windows.Forms.GroupBox();
            this.btn_uncompactify = new System.Windows.Forms.Button();
            this.btn_compactify = new System.Windows.Forms.Button();
            this.lbl_processing = new System.Windows.Forms.Label();
            this.dlg_selectdir = new System.Windows.Forms.FolderBrowserDialog();
            this.txt_info_processing = new System.Windows.Forms.TextBox();
            this.txt_info_selected = new System.Windows.Forms.TextBox();
            this.grp_step1.SuspendLayout();
            this.grp_step2.SuspendLayout();
            this.grp_step3.SuspendLayout();
            this.SuspendLayout();
            // 
            // grp_step1
            // 
            this.grp_step1.Controls.Add(this.txt_info_selected);
            this.grp_step1.Controls.Add(this.lbl_selected);
            this.grp_step1.Controls.Add(this.btn_selectdir);
            this.grp_step1.Location = new System.Drawing.Point(12, 12);
            this.grp_step1.Name = "grp_step1";
            this.grp_step1.Size = new System.Drawing.Size(263, 100);
            this.grp_step1.TabIndex = 0;
            this.grp_step1.TabStop = false;
            this.grp_step1.Text = "Step 1";
            // 
            // lbl_selected
            // 
            this.lbl_selected.AutoSize = true;
            this.lbl_selected.Location = new System.Drawing.Point(6, 45);
            this.lbl_selected.Name = "lbl_selected";
            this.lbl_selected.Size = new System.Drawing.Size(95, 13);
            this.lbl_selected.TabIndex = 1;
            this.lbl_selected.Text = "Selected directory:";
            // 
            // btn_selectdir
            // 
            this.btn_selectdir.Location = new System.Drawing.Point(6, 19);
            this.btn_selectdir.Name = "btn_selectdir";
            this.btn_selectdir.Size = new System.Drawing.Size(251, 23);
            this.btn_selectdir.TabIndex = 0;
            this.btn_selectdir.Text = "Select directory";
            this.btn_selectdir.UseVisualStyleBackColor = true;
            this.btn_selectdir.Click += new System.EventHandler(this.btn_selectdir_Click);
            // 
            // grp_step2
            // 
            this.grp_step2.Controls.Add(this.lbl_compression);
            this.grp_step2.Controls.Add(this.rad_compression_insane);
            this.grp_step2.Controls.Add(this.rad_compression_high);
            this.grp_step2.Controls.Add(this.rad_compression_medium);
            this.grp_step2.Controls.Add(this.rad_compression_low);
            this.grp_step2.Location = new System.Drawing.Point(12, 118);
            this.grp_step2.Name = "grp_step2";
            this.grp_step2.Size = new System.Drawing.Size(263, 126);
            this.grp_step2.TabIndex = 1;
            this.grp_step2.TabStop = false;
            this.grp_step2.Text = "Step 2";
            // 
            // lbl_compression
            // 
            this.lbl_compression.AutoSize = true;
            this.lbl_compression.Location = new System.Drawing.Point(7, 16);
            this.lbl_compression.Name = "lbl_compression";
            this.lbl_compression.Size = new System.Drawing.Size(125, 13);
            this.lbl_compression.TabIndex = 4;
            this.lbl_compression.Text = "Select compression type:";
            // 
            // rad_compression_insane
            // 
            this.rad_compression_insane.AutoSize = true;
            this.rad_compression_insane.Location = new System.Drawing.Point(10, 101);
            this.rad_compression_insane.Name = "rad_compression_insane";
            this.rad_compression_insane.Size = new System.Drawing.Size(175, 17);
            this.rad_compression_insane.TabIndex = 3;
            this.rad_compression_insane.TabStop = true;
            this.rad_compression_insane.Text = "Insane (FOR STORAGE ONLY)";
            this.rad_compression_insane.UseVisualStyleBackColor = true;
            // 
            // rad_compression_high
            // 
            this.rad_compression_high.AutoSize = true;
            this.rad_compression_high.Checked = true;
            this.rad_compression_high.Location = new System.Drawing.Point(10, 78);
            this.rad_compression_high.Name = "rad_compression_high";
            this.rad_compression_high.Size = new System.Drawing.Size(93, 17);
            this.rad_compression_high.TabIndex = 2;
            this.rad_compression_high.TabStop = true;
            this.rad_compression_high.Text = "High (Slowest)";
            this.rad_compression_high.UseVisualStyleBackColor = true;
            // 
            // rad_compression_medium
            // 
            this.rad_compression_medium.AutoSize = true;
            this.rad_compression_medium.Location = new System.Drawing.Point(10, 55);
            this.rad_compression_medium.Name = "rad_compression_medium";
            this.rad_compression_medium.Size = new System.Drawing.Size(62, 17);
            this.rad_compression_medium.TabIndex = 1;
            this.rad_compression_medium.TabStop = true;
            this.rad_compression_medium.Text = "Medium";
            this.rad_compression_medium.UseVisualStyleBackColor = true;
            // 
            // rad_compression_low
            // 
            this.rad_compression_low.AutoSize = true;
            this.rad_compression_low.Location = new System.Drawing.Point(10, 32);
            this.rad_compression_low.Name = "rad_compression_low";
            this.rad_compression_low.Size = new System.Drawing.Size(88, 17);
            this.rad_compression_low.TabIndex = 0;
            this.rad_compression_low.TabStop = true;
            this.rad_compression_low.Text = "Low (Fastest)";
            this.rad_compression_low.UseVisualStyleBackColor = true;
            // 
            // grp_step3
            // 
            this.grp_step3.Controls.Add(this.txt_info_processing);
            this.grp_step3.Controls.Add(this.btn_uncompactify);
            this.grp_step3.Controls.Add(this.btn_compactify);
            this.grp_step3.Controls.Add(this.lbl_processing);
            this.grp_step3.Location = new System.Drawing.Point(12, 250);
            this.grp_step3.Name = "grp_step3";
            this.grp_step3.Size = new System.Drawing.Size(263, 133);
            this.grp_step3.TabIndex = 2;
            this.grp_step3.TabStop = false;
            this.grp_step3.Text = "Step 3";
            // 
            // btn_uncompactify
            // 
            this.btn_uncompactify.Enabled = false;
            this.btn_uncompactify.Location = new System.Drawing.Point(6, 48);
            this.btn_uncompactify.Name = "btn_uncompactify";
            this.btn_uncompactify.Size = new System.Drawing.Size(251, 23);
            this.btn_uncompactify.TabIndex = 6;
            this.btn_uncompactify.Text = "Un-Compactify!";
            this.btn_uncompactify.UseVisualStyleBackColor = true;
            this.btn_uncompactify.Click += new System.EventHandler(this.btn_uncompactify_Click);
            // 
            // btn_compactify
            // 
            this.btn_compactify.Enabled = false;
            this.btn_compactify.Location = new System.Drawing.Point(6, 19);
            this.btn_compactify.Name = "btn_compactify";
            this.btn_compactify.Size = new System.Drawing.Size(251, 23);
            this.btn_compactify.TabIndex = 3;
            this.btn_compactify.Text = "Compactify!";
            this.btn_compactify.UseVisualStyleBackColor = true;
            this.btn_compactify.Click += new System.EventHandler(this.btn_compactify_Click);
            // 
            // lbl_processing
            // 
            this.lbl_processing.AutoSize = true;
            this.lbl_processing.Location = new System.Drawing.Point(6, 74);
            this.lbl_processing.Name = "lbl_processing";
            this.lbl_processing.Size = new System.Drawing.Size(105, 13);
            this.lbl_processing.TabIndex = 4;
            this.lbl_processing.Text = "Currently processing:";
            // 
            // txt_info_processing
            // 
            this.txt_info_processing.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txt_info_processing.Location = new System.Drawing.Point(6, 90);
            this.txt_info_processing.Multiline = true;
            this.txt_info_processing.Name = "txt_info_processing";
            this.txt_info_processing.ReadOnly = true;
            this.txt_info_processing.Size = new System.Drawing.Size(251, 37);
            this.txt_info_processing.TabIndex = 7;
            this.txt_info_processing.Text = "N/A";
            // 
            // txt_info_selected
            // 
            this.txt_info_selected.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txt_info_selected.Location = new System.Drawing.Point(6, 57);
            this.txt_info_selected.Multiline = true;
            this.txt_info_selected.Name = "txt_info_selected";
            this.txt_info_selected.ReadOnly = true;
            this.txt_info_selected.Size = new System.Drawing.Size(251, 37);
            this.txt_info_selected.TabIndex = 8;
            this.txt_info_selected.Text = "N/A";
            // 
            // frm_main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(287, 395);
            this.Controls.Add(this.grp_step3);
            this.Controls.Add(this.grp_step2);
            this.Controls.Add(this.grp_step1);
            this.Name = "frm_main";
            this.Text = "Compactify";
            this.grp_step1.ResumeLayout(false);
            this.grp_step1.PerformLayout();
            this.grp_step2.ResumeLayout(false);
            this.grp_step2.PerformLayout();
            this.grp_step3.ResumeLayout(false);
            this.grp_step3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox grp_step1;
        private System.Windows.Forms.Label lbl_selected;
        private System.Windows.Forms.Button btn_selectdir;
        private System.Windows.Forms.GroupBox grp_step2;
        private System.Windows.Forms.Label lbl_compression;
        private System.Windows.Forms.RadioButton rad_compression_insane;
        private System.Windows.Forms.RadioButton rad_compression_high;
        private System.Windows.Forms.RadioButton rad_compression_medium;
        private System.Windows.Forms.RadioButton rad_compression_low;
        private System.Windows.Forms.GroupBox grp_step3;
        private System.Windows.Forms.Button btn_compactify;
        private System.Windows.Forms.Label lbl_processing;
        private System.Windows.Forms.FolderBrowserDialog dlg_selectdir;
        private System.Windows.Forms.Button btn_uncompactify;
        private System.Windows.Forms.TextBox txt_info_selected;
        private System.Windows.Forms.TextBox txt_info_processing;
    }
}

