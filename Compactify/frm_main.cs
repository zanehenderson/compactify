﻿using System;
using System.Windows.Forms;

namespace Compactify
{
    public partial class frm_main : Form
    {
        private string _selectedDir = "";

        public frm_main()
        {
            InitializeComponent();
        }

        private void btn_selectdir_Click(object sender, EventArgs e)
        {
            // Display the openFile dialog.
            DialogResult result = dlg_selectdir.ShowDialog();

            // OK button was pressed.
            if (result == DialogResult.OK)
            {
                string dir = dlg_selectdir.SelectedPath;
                if (dir == "C:\\" || dir.StartsWith("C:\\Windows"))
                {
                    MessageBox.Show("That's not a good idea.\nChoose another.");
                }
                else
                {
                    _selectedDir = dir;
                    txt_info_selected.Text = dir;
                    btn_compactify.Enabled = true;
                    btn_uncompactify.Enabled = true;
                }
            }

            // Cancel button was pressed.
            else if (result == DialogResult.Cancel)
            {
                return;
            }
        }

        private void btn_compactify_Click(object sender, EventArgs e)
        {
            string alg = "";
            if (rad_compression_low.Checked)
                alg = "/EXE:XPRESS4K";
            else if (rad_compression_medium.Checked)
                alg = "/EXE:XPRESS8K";
            else if (rad_compression_high.Checked)
                alg = "/EXE:XPRESS16K";
            else if (rad_compression_insane.Checked)
                alg = "/EXE:LZX";

            FileWalker.Start(_selectedDir, txt_info_processing, "/C", alg);
        }

        private void btn_uncompactify_Click(object sender, EventArgs e)
        {
            FileWalker.Start(_selectedDir, txt_info_processing, "/U", "");
        }
    }
}
